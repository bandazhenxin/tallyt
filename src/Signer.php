<?php
namespace bdhert\Tally;

/**
 * Class Signer
 * @package bdhert\Tally
 */
abstract class Signer {
    protected Config $config;

    public function __construct(Config $config) {
        $this->config = $config;
    }

    abstract function check(string $sign_params, string $sign): bool;
}
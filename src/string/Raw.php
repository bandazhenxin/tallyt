<?php
namespace bdhert\Tally\string;

/**
 * 获取原数据
 * Class Raw
 * @package bdhert\Tally\string
 */
class Raw implements Handler {
    public function refit (string $data): string {
        return trim($data);
    }
}
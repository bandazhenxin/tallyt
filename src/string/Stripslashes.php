<?php
namespace bdhert\Tally\string;

/**
 * 转义符处理
 * Class Stripslashes
 * @package bdhert\Tally\string
 */
class Stripslashes extends Decorator {
    public function refit (string $data): string {
        return stripslashes($this->handler->refit($data));
    }
}
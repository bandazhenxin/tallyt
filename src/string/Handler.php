<?php
namespace bdhert\Tally\string;

/**
 * 整装说明书
 * Interface Handler
 * @package bdhert\Tally\string
 */
interface Handler {
    /**
     * 数据整装
     * @param string $data
     * @return string
     */
    public function refit (string $data): string;
}
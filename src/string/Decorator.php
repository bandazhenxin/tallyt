<?php
namespace bdhert\Tally\string;

/**
 * 字符数据装饰器
 * Class Decorator
 * @package bdhert\Tally\string
 */
abstract class Decorator implements Handler {
    protected Handler $handler;

    public function __construct (Handler $handler) {
        $this->handler = $handler;
    }
}
<?php
namespace bdhert\Tally\string;

/**
 * html字符实体处理
 * Class Specialchars
 * @package bdhert\Tally\string
 */
class Specialchars extends Decorator {
    public function refit (string $data): string {
        return htmlspecialchars_decode($this->handler->refit($data));
    }
}
<?php
namespace bdhert\Tally;

use Webman\Http\Request;

/**
 * 参与签名参数
 * Class Joiner
 * @package bdhert\Tally
 */
abstract class Joiner {
    protected Request $request;
    protected Config  $config;

    public function __construct(Config $config, Request $request) {
        [$this->request, $this->config] = [$request, $config];
    }

    abstract public function getData(): array;
}
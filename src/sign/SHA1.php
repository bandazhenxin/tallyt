<?php
namespace bdhert\Tally\sign;

use bdhert\Tally\Signer;

/**
 * sha1签名
 * Class SHA1
 * @package bdhert\Tally\sign
 */
class SHA1 extends Signer {
    public function check(string $sign_params, string $sign): bool {
        return $sign === sha1("{$sign_params}.{$this->config['secret']}");
    }
}
<?php
namespace bdhert\Tally\sign;

use bdhert\Tally\Config;
use bdhert\Tally\Signer;
use bdhert\Tally\exception\SignOptionsException;

/**
 * 根据配置获取签名实例
 * Class Sign
 * @package bdhert\Tally\sign
 */
class Sign {
    /**
     * 根据配置获取签名实例
     * @param Config $config
     * @return Signer
     */
    public static function Opt(Config $config) {
        $algo  = strtoupper($config['algo']);
        $class = __NAMESPACE__ . "\\{$algo}";

        try {
            $signer = (fn (Signer $signer): Signer => $signer)(new $class($config));
        } catch (\Exception $e) {
            throw new SignOptionsException('签名方法配置错误', 500);
        }

        return $signer;
    }
}
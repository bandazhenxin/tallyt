<?php
namespace bdhert\Tally\sign;

use bdhert\Tally\Signer;

/**
 * MD5签名
 * Class MD5
 * @package bdhert\Tally\sign
 */
class MD5 extends Signer {
    public function check(string $sign_params, string $sign): bool {
        return $sign === strtoupper(md5("{$sign_params}.{$this->config['secret']}"));
    }
}
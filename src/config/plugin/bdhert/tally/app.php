<?php
return [
    'enable'   => true,
    'default'  => 'api',
    'channels' => [
        'api' => [
            'algo'        => 'md5',
            'ttl'         => 5,
            'future'      => 60,
            'sign_key'    => 'X-Sign',
            'join_mode'   => '',
            'join_header' => '',
            'time_key'    => 'X-Time',
            'nonce_key'   => 'X-Nonce',
            'public_key'  => 'file://path/public.key',
            'secret'      => 'SIvEH7eNr3zIdkio'
        ]
    ],
];
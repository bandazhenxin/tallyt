<?php
namespace bdhert\Tally;

use bdhert\Tally\exception\SignOptionsException;

/**
 * 验签配置
 * Class Config
 * @package bdhert\Tally
 */
class Config implements \ArrayAccess {
    public array $supported_algo = ['md5', 'sha1', 'rsa'];

    public string $channel = 'api';
    public    int     $ttl = 0;
    public string    $algo = 'md5';

    public function __construct(string $channel = '') {
        if (!empty($channel)) {
            $this->channel = $channel;
        } else {
            $channel = config('plugin.bdhert.tally.app.default');
        }

        if (empty($channel)) throw new SignOptionsException('渠道配置错误', 500);

        $options = config("plugin.bdhert.tally.app.channels.{$channel}");
        if (!empty($options)) {
            foreach ($options as $key => $value) {
                $this->$key = $value;
            }
        } else {
            throw new SignOptionsException('配置项错误', 500);
        }

        // algo 验证
        if (!in_array($this->algo, $this->supported_algo))
            throw new SignOptionsException('加密方法错误', 500);
    }

    public function offsetExists($offset) {
        return isset($this->$offset);
    }

    public function offsetGet($offset) {
        return $this->$offset;
    }

    public function offsetSet($offset, $value) {
        $this->$offset = $value;
    }

    public function offsetUnset($offset) {
        unset($this->$offset);
    }
}
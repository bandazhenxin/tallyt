<?php
namespace bdhert\Tally\join;

use bdhert\Tally\Joiner;

/**
 * 复合模式
 * Class Params
 * @package bdhert\Tally\join
 */
class Params extends Joiner {
    public function getData(): array {
        $method = strtoupper($this->request->method());

        $get_method  = ['GET', 'DELETE'];
        $post_method = ['POST', 'PUT'];

        if (in_array($method, $get_method)) {
            return $this->request->get();
        } else if (in_array($method, $post_method)) {
            return $this->request->post();
        }

        return [];
    }
}
<?php
namespace bdhert\Tally\join;

use bdhert\Tally\Joiner;

/**
 * 头部参数签名模式
 * Class Header
 * @package bdhert\Tally\join
 */
class Header extends Joiner {
    public function getData(): array {
        if (empty($join_header = $this->config['join_header'])) return [];

        return [$join_header => $this->request->header($join_header)];
    }
}
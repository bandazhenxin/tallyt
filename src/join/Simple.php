<?php
namespace bdhert\Tally\join;

use bdhert\Tally\Joiner;

/**
 * 默认简易模式
 * Class Simple
 * @package bdhert\Tally\join
 */
class Simple extends Joiner {
    public function getData(): array {
        return [];
    }
}
<?php
namespace bdhert\Tally\join;

use bdhert\Tally\Config;
use bdhert\Tally\exception\SignOptionsException;
use bdhert\Tally\Joiner;
use bdhert\Tally\Signer;
use Webman\Http\Request;

/**
 * 获取签名参与实例
 * Class JoinMode
 * @package bdhert\Tally\join
 */
class JoinMode {
    /**
     * 根据配置获取签名实例
     * @param Config $config
     * @return Signer
     */
    public static function Opt(Config $config, Request $request): Joiner {
        $join  = ucfirst(strtolower($config['join_mode'] ?? 'simple'));
        $class = __NAMESPACE__ . "\\{$join}";

        try {
            $joiner = (fn (Joiner $joiner): Joiner => $joiner)(new $class($config, $request));
        } catch (\Exception $e) {
            throw new SignOptionsException('签名方法配置错误', 500);
        }

        return $joiner;
    }
}
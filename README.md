# tally

## 说明

#### 介绍
时效签名，api sign，一款简易、好用、有时效验证的签名验证包

#### 架构
基于PHP的webman框架

#### 思想
* 安全不是绝对的，安全就是“根据不同程度的需求做出对应破解难度门槛的设计”。api安全只要能达到破解者必须通过阅读源码、打断点的方式破解，那么我就认为它是安全的，所以我这里不建议使用H5项目。另一方面破解者还有自动化测试、模拟机器人的方式进行破解，这里建议实现限流器进行有限的限制，当然更有效的安全处理方式是使用“用户行为识别”，这就超出api可设计的范围了
* 使用原则
> 1.  我认为api签名必须有时限，否则不能从根本上防止“重放”；
> 2.  签名的加密方法是消费端与服务端都需要实现的，所以为了降低开发难度，我的加密方法只使用'md5'、'sha1'、'rsa'；
> 3.  GET、DELETE方法只有query参数参与校验，POST、PUT方法只有body参数参与校验。


## 使用

#### 安装

1.  composer require bdhert/tally
2.  开发版：composer require bdhert/tally:"dev-master"
3.  基础版：composer require bdhert/tally:"^0.1"

#### 配置

```php

<?php
return [
    'enable'   => true,
    'default'  => 'api',
    'channels' => [
        'api' => [
            'algo'       => 'md5',
            'ttl'        => 5,
            'sign_key'   => 'X-Sign',
            'time_key'   => 'X-Time',
            'nonce_key'  => 'X-Nonce',
            'public_key' => 'file://path/public.key',
            'secret'     => 'SIvEH7eNr3zIdkio'
        ]
    ],
];

```
本包适用多客户端验签以上是默认生成的配置文件，
1、channels通道的键名作为渠道名，在实例化的时候传入具体的渠道名；
2、当ttl设置为0时表示不进行时效验证，虽然我们提供这个选择但我不建议这么做，否则api签名在某种意义上来说是无意义的；
3、algo选项选择签名函数，这是需要跟客户端开发者商议好的、各方都需要实现的方式，目前可选择项包括'md5', 'sha1', 'rsa'。

#### 使用

```php

<?php
use bdhert\Tally\exception\SignException;
use bdhert\Tally\exception\SignExpiredException;
use bdhert\Tally\exception\SignInvalidException;
use bdhert\Tally\exception\SignOptionsException;

try {
    if (!(new APISign('api'))->check()) throw new SignException('签名错误', 402);
} catch (SignExpiredException $e) {
    throw new SignException('签名已过期', 402);
} catch (SignOptionsException $e) {
    throw new SignException('签名配置错误', 402);
} catch (SignInvalidException $e) {
    throw new SignException('签名错误', 402);
} catch (SignException $e) {
    throw new SignException('签名错误', 402);
}

```
